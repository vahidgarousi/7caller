package com.sevenlearn.app.sevencaller.framework.lang

abstract class Lang {
    open val button_yes = "yes"
    open val button_no = "no"
    open val button_cancel = "CANCEL"
    open val button_ok = "OK"
    open val permissionRequiredMessage =
        "Please grant all permission required.\nIf not, app won't work normally."

    open fun getCurrentPattern(pattern: String): String {
        return "Current Pattern: $pattern"
    }

    open fun getCurrentVersion(versionCode: Int): String = "Version: $versionCode"

    open val currentPattern: String = "Current Pattern: "
    open val customer_number_is_empty: String = "Please enter valid phone number"

    open val pattern_saved: String = "Pattern Saved!"
    open val phone_variable_required: String = "Please use %phone% in your pattern"
    open val pattern_required: String = "Please enter your pattern"
}