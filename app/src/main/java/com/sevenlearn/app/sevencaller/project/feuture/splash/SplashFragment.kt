package com.sevenlearn.app.sevencaller.project.feuture.splash

import androidx.navigation.fragment.findNavController
import com.sevenlearn.app.sevencaller.BuildConfig
import com.sevenlearn.app.sevencaller.R
import com.sevenlearn.app.sevencaller.databinding.FragmentSplashBinding
import com.sevenlearn.app.sevencaller.framework.bace.BaseFragment
import com.sevenlearn.app.sevencaller.framework.core.App
import com.sevenlearn.app.sevencaller.framework.core.L

class SplashFragment : BaseFragment() {
    private lateinit var splashBinding: FragmentSplashBinding
    override val getLayoutRes: Int = R.layout.fragment_splash

    override fun setupViews() {
        splashBinding = FragmentSplashBinding.bind(requireView())
        splashBinding.tvFragmentSplashVersion.setText(L.getCurrentVersion(BuildConfig.VERSION_CODE))
        App.handler.postDelayed({
            findNavController().navigate(R.id.action_splashFragment_to_homeFragment)
        }, 1500)
    }
}