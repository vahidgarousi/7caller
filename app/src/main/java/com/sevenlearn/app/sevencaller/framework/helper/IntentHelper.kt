package com.sevenlearn.app.sevencaller.framework.helper

import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.net.Uri
import android.provider.Settings
import com.sevenlearn.app.sevencaller.framework.bace.BaseActivity
import com.sevenlearn.app.sevencaller.framework.bace.MethodBlock

class IntentHelper {
    companion object {
        var lastPendingIntentId = 0
        fun createPendingIntent(
            context: Context,
            clazz: Class<out BaseActivity>,
            props: MethodBlock<Intent>
        ): PendingIntent {
            val intent = Intent(context, clazz)
            intent.apply(props)
            val pendingIntent = PendingIntent.getActivity(
                context,
                +lastPendingIntentId,
                intent,
                PendingIntent.FLAG_UPDATE_CURRENT
            )
            return pendingIntent
        }

        fun openAppSettings(context: Context) {
            val intent = Intent()
            intent.action = Settings.ACTION_APPLICATION_DETAILS_SETTINGS
            val uri = Uri.fromParts("package", context.packageName, null)
            intent.data = uri
            context.startActivity(intent)
        }
    }
}