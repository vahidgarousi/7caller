package com.sevenlearn.app.sevencaller.framework.helper

import android.content.Context
import android.content.Intent
import android.net.Uri
import android.widget.Toast


object SystemHelper {
    fun callNumber(context: Context, phone: String) {
        Debug.info("ussd ==> " + phone)
        try {
            val callIntent = Intent(Intent.ACTION_CALL)
            callIntent.setPackage("com.android.server.telecom")
            callIntent.data = ussdToCallableUri(phone)
            context.startActivity(callIntent)
        } catch (e: SecurityException) {
            Toast.makeText(context, "Need call permission" + e.message, Toast.LENGTH_LONG).show()
        } catch (e: Exception) {
            e.printStackTrace()
            Toast.makeText(context, "No SIM Found" + e.message, Toast.LENGTH_LONG).show()
        }
    }


    private fun ussdToCallableUri(ussd: String): Uri? {
        var uriString: String? = ""
        if (!ussd.startsWith("tel:")) uriString += "tel:"
        for (c in ussd.toCharArray()) {
            if (c == '#') uriString += Uri.encode("#") else uriString += c
        }
        return Uri.parse(uriString)
    }
}