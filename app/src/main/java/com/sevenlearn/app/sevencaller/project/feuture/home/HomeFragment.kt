package com.sevenlearn.app.sevencaller.project.feuture.home

import android.text.TextUtils
import android.view.View
import android.widget.TextView
import com.sevenlearn.app.sevencaller.R
import com.sevenlearn.app.sevencaller.databinding.FragmentHomeBinding
import com.sevenlearn.app.sevencaller.framework.bace.BaseFragment
import com.sevenlearn.app.sevencaller.framework.bace.getActivity
import com.sevenlearn.app.sevencaller.framework.bace.vibratePhone
import com.sevenlearn.app.sevencaller.framework.core.L
import com.sevenlearn.app.sevencaller.framework.helper.Debug
import com.sevenlearn.app.sevencaller.framework.helper.SystemHelper
import kotlinx.android.synthetic.main.fragment_home.*

class HomeFragment : BaseFragment(), View.OnClickListener {

    private lateinit var homeBinding: FragmentHomeBinding
    private var phone: String = ""
    private lateinit var dialogSetting: DialogSetting

    private lateinit var settingManager: SettingManager
    override val getLayoutRes: Int = R.layout.fragment_home

    override fun setupViews() {
        homeBinding = FragmentHomeBinding.bind(requireView())
        settingManager = SettingManager(requireContext())
        dialogSetting = DialogSetting()
        homeBinding.btnBackSpace.setOnClickListener {
            if (phone.length > 0) {
                phone = phone.substring(0, phone.length - 1)
                homeBinding.edtFragmentHomePhone.setText(phone)
            }
        }
        checkPattern()
        val tvPattern = requireView().findViewById<TextView>(R.id.edt_fragmentHome_pattern)
        tvPattern.hint = L.currentPattern
        tvPattern.text = settingManager.pattern()

        homeBinding.btn1.setOnClickListener(this)
        homeBinding.btn2.setOnClickListener(this)
        homeBinding.btn3.setOnClickListener(this)
        homeBinding.btn4.setOnClickListener(this)
        homeBinding.btn5.setOnClickListener(this)
        homeBinding.btn6.setOnClickListener(this)
        homeBinding.btn7.setOnClickListener(this)
        homeBinding.btn8.setOnClickListener(this)
        homeBinding.btn9.setOnClickListener(this)
        homeBinding.btn0.setOnClickListener(this)
        homeBinding.btnStar.setOnClickListener {
            phone += "*"
            edt_fragmentHome_phone.setText(phone)
        }
        homeBinding.btnSquare.setOnClickListener {
            phone += "#"
            edt_fragmentHome_phone.setText(phone)
        }
        btn_fragmentHome_setting.setOnClickListener {
            showPatternSettingDialog(tvPattern)
        }
        homeBinding.btnSavePattern.setOnClickListener {
            homeBinding.edtFragmentHomePattern.run {
                if (!this.text.toString().contains("%phone%")) {
                    this.setError(L.phone_variable_required)
                } else if (TextUtils.isEmpty(this.text.toString())) {
                    this.setError(L.pattern_required)
                } else {
                    getActivity.showToast(L.pattern_saved)
                    settingManager.savePattern(this.text.toString().trim())
                    this.hint = L.currentPattern
                    this.setText(settingManager.pattern())
                }
            }

        }
        homeBinding.btnCall.setOnClickListener {
//            val customerPhone = homeBinding.edtFragmentHomePhone.text.toString().trim()
            Debug.info("Phone is => $phone")
            if (settingManager.pattern() == null) {
                showPatternSettingDialog(tvPattern)
            } else if (TextUtils.isEmpty(phone)) {
                getActivity.showToast(L.customer_number_is_empty)
                return@setOnClickListener
            }
            val ussd = settingManager.pattern()!!
                .replace("%phone%", homeBinding.edtFragmentHomePhone.text.toString().trim())
            Debug.info("ussd ==> " + ussd)
            SystemHelper.callNumber(requireContext(), ussd)

        }
    }

    private fun checkPattern() {
        if (settingManager.pattern() == null) {
            showPatternSettingDialog(edt_fragmentHome_pattern)
        }
    }

    private fun showPatternSettingDialog(tvPattern: TextView) {
        dialogSetting.apply {
            onSave = {
                getActivity.showToast(L.pattern_saved)
                settingManager.savePattern(this)
                tvPattern.hint = L.currentPattern
                tvPattern.text = settingManager.pattern()
                dialogSetting.dismiss()
            }
            isCancelable = false
        }
        dialogSetting.show(getActivity.supportFragmentManager, null)
    }

    override fun onClick(v: View?) {
        v?.let { appendNumber(it) }
    }


    private fun appendNumber(view: View) {
        vibratePhone()
        val number = resources.getResourceEntryName(view.id).substringAfter('_')
        phone += number
        edt_fragmentHome_phone.setText(phone)
    }


}