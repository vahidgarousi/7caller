package com.sevenlearn.app.sevencaller.framework.core

import android.app.Application
import android.os.Handler
import com.sevenlearn.app.sevencaller.framework.lang.EnLang
import com.sevenlearn.app.sevencaller.framework.lang.Lang

val L: Lang = EnLang()

class App : Application() {

    companion object {
        lateinit var handler: Handler
    }

    override fun onCreate() {
        super.onCreate()
        handler = Handler()
    }
}