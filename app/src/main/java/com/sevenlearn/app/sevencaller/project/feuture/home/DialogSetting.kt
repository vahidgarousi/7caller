package com.sevenlearn.app.sevencaller.project.feuture.home

import android.text.TextUtils
import android.view.View
import com.google.android.material.button.MaterialButton
import com.google.android.material.textfield.TextInputEditText
import com.sevenlearn.app.sevencaller.R
import com.sevenlearn.app.sevencaller.framework.bace.BaseDialog
import com.sevenlearn.app.sevencaller.framework.bace.MethodBlock
import com.sevenlearn.app.sevencaller.framework.core.L

class DialogSetting constructor(var onSave: MethodBlock<String> = {}) : BaseDialog() {
    override val getDialogLayout: Int = R.layout.dialog_setting
    override val dialogTheme: Int = R.style.MaterialAlertDialogCustom

    // UI  Variables
    private lateinit var btnSave: MaterialButton
    private lateinit var edtPattern: TextInputEditText


    override fun setupViews(view: View) {
        btnSave = view.findViewById(R.id.btn_dialogSetting_save)
        edtPattern = view.findViewById(R.id.edt_dialogSetting_pattern)
        edtPattern.setText(SettingManager(requireContext()).pattern())
        btnSave.setOnClickListener {
            if (!edtPattern.text.toString().contains("%phone%")) {
                edtPattern.setError(L.phone_variable_required)
            } else if (TextUtils.isEmpty(edtPattern.text.toString())) {
                edtPattern.setError(L.pattern_required)
            } else {
                onSave(edtPattern.text.toString().trim())
            }
        }
    }

    override fun afterOnActivityCreated() {
//        dialog!!.window!!.attributes.windowAnimations = R.style.DialogAnimation
        dialog!!.window!!.setBackgroundDrawable(null)
    }
}