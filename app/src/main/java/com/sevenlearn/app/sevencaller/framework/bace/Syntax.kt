package com.sevenlearn.app.sevencaller.framework.bace

import android.content.Context
import android.content.SharedPreferences
import android.os.Build
import android.os.VibrationEffect
import android.os.Vibrator
import android.preference.PreferenceManager
import androidx.fragment.app.Fragment

typealias MethodBlock<T> = T.() -> Unit
typealias FunctionBlock = () -> Unit

typealias AndroidPermission = android.Manifest.permission

inline val Context.defaultSharedPreferences: SharedPreferences
    get() = PreferenceManager.getDefaultSharedPreferences(
        this
    )

inline val BaseFragment.getActivity: BaseActivity get() = (activity as BaseActivity)


fun Fragment.vibratePhone() {
    val vibrator = context?.getSystemService(Context.VIBRATOR_SERVICE) as Vibrator
    if (Build.VERSION.SDK_INT >= 26) {
        vibrator.vibrate(VibrationEffect.createOneShot(100, VibrationEffect.DEFAULT_AMPLITUDE))
    } else {
        vibrator.vibrate(100)
    }
}