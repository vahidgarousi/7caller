package com.sevenlearn.app.sevencaller.framework.bace

import android.app.Dialog
import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.widget.Toast
import androidx.annotation.LayoutRes
import androidx.annotation.StyleRes
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import com.google.android.material.dialog.MaterialAlertDialogBuilder

abstract class BaseDialog(protected val props: MethodBlock<MaterialAlertDialogBuilder> = {}) :
    BottomSheetDialogFragment() {
    @get:LayoutRes
    abstract val getDialogLayout: Int

    @get:StyleRes
    abstract val dialogTheme: Int

    abstract fun afterOnActivityCreated()

    abstract fun setupViews(view: View)

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val alertBuilder = MaterialAlertDialogBuilder(requireContext(), dialogTheme)
        val view = LayoutInflater.from(requireContext()).inflate(getDialogLayout, null, false)
        setupViews(view)
        alertBuilder.setView(view)
        return alertBuilder.create()
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        afterOnActivityCreated()
    }

    fun showToast(context: Context, message: Any, length: Int) =
        Toast.makeText(context, "" + message, length).show()
}