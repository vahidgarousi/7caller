package com.sevenlearn.app.sevencaller.framework.bace

import android.os.Bundle
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.google.android.material.snackbar.Snackbar
import com.sevenlearn.app.sevencaller.framework.handler.PermissionHandler
import com.sevenlearn.app.sevencaller.framework.helper.Debug

abstract class BaseActivity : AppCompatActivity() {
    private val permissionHandlers = mutableListOf<PermissionHandler>()

    fun showSnackBar(message: String) {
        Snackbar.make(findViewById(getCoordinatorLayout()), message, Snackbar.LENGTH_LONG).show()
    }


    fun showToast(message: Any) {
        Toast.makeText(applicationContext, "" + message, Toast.LENGTH_LONG).show()
    }

    fun showToast(message: Any, length: Int) {
        Toast.makeText(applicationContext, "" + message, length).show()
    }

    abstract fun getCoordinatorLayout(): Int

    fun addPermissionHandler(permissionHandler: PermissionHandler) {
        permissionHandlers.add(permissionHandler)
    }


    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        for (permissionHandler in permissionHandlers) {
            val handled =
                permissionHandler.processOnPermissionResult(requestCode, permissions, grantResults)
            if (handled) {
                return
            }
        }
    }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        Debug.level = Debug.LOG_LEVEL.VERBOSE
        Debug.logTag = "7Caller"
    }
}