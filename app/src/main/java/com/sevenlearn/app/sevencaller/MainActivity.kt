package com.sevenlearn.app.sevencaller

import android.os.Bundle
import androidx.lifecycle.LiveData
import androidx.lifecycle.Observer
import androidx.navigation.NavController
import com.google.android.material.bottomnavigation.BottomNavigationView
import com.sevenlearn.app.sevencaller.databinding.ActivityMainBinding
import com.sevenlearn.app.sevencaller.framework.bace.AndroidPermission
import com.sevenlearn.app.sevencaller.framework.bace.BaseActivity
import com.sevenlearn.app.sevencaller.framework.bace.setupWithNavController
import com.sevenlearn.app.sevencaller.framework.core.L
import com.sevenlearn.app.sevencaller.framework.handler.PermissionHandler
import com.sevenlearn.app.sevencaller.framework.helper.Debug

class MainActivity : BaseActivity() {
    lateinit var binding: ActivityMainBinding
    private var currentNavController: LiveData<NavController>? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        val view = binding.root
        setContentView(view)
        PermissionHandler(
            this, listOf(
                AndroidPermission.CALL_PHONE
            )
        ) {
            whyPermissionRequired = L.permissionRequiredMessage
            onGrant = {
                Debug.info("Client Granted second")
            }
        }
        if (savedInstanceState == null) {
            setupBottomNavigationBar()
        } // Else, need to wait for onRestoreInstanceState

    }

    override fun onRestoreInstanceState(savedInstanceState: Bundle?) {
        super.onRestoreInstanceState(savedInstanceState)
        // Now that BottomNavigationBar has restored its instance state
        // and its selectedItemId, we can proceed with setting up the
        // BottomNavigationBar with Navigation
        setupBottomNavigationBar()
    }

    /**
     * Called on first creation and when restoring state.
     */
    private fun setupBottomNavigationBar() {
        val bottomNavigationView =
            findViewById<BottomNavigationView>(binding.bnvActivityMainMain.id)

        val navGraphIds = listOf(
            R.navigation.home
        )

        // Setup the bottom navigation view with a list of navigation graphs
        val controller = bottomNavigationView.setupWithNavController(
            navGraphIds = navGraphIds,
            fragmentManager = supportFragmentManager,
            containerId = binding.fcvActivityMainMain.id,
            intent = intent
        )

        // Whenever the selected controller changes, setup the action bar.
        controller.observe(this, Observer { navController ->
//      setupActionBarWithNavController(navController)
        })
        currentNavController = controller
    }

    override fun getCoordinatorLayout(): Int {
        return R.id.cl_activityMain_main
    }

    override fun onSupportNavigateUp(): Boolean {
        return currentNavController?.value?.navigateUp() ?: false
    }
}