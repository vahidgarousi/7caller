package com.sevenlearn.app.sevencaller.project.feuture.home

import android.content.Context
import android.content.SharedPreferences

class SettingManager(context: Context) {
    private val sharedPreferences: SharedPreferences

    init {
        sharedPreferences = context.getSharedPreferences("app_info", Context.MODE_PRIVATE)
    }

    fun savePattern(pattern: String) {
        val editor = sharedPreferences.edit()
        editor.putString("pattern", pattern)
        editor.apply()
    }

    fun pattern(): String? {
        return sharedPreferences.getString("pattern", null)
    }
}