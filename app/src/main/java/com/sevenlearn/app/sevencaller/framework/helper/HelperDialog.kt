package com.sevenlearn.app.sevencaller.framework.bace

import android.content.Context
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import com.sevenlearn.app.sevencaller.R
import com.sevenlearn.app.sevencaller.framework.core.L

class HelperDialog constructor(var context: Context) {

    companion object {
        // MaterialAlertDialogBuilder
        fun alertYesNoNew(
            context: Context,
            message: String,
            title: String? = null,
            onYes: FunctionBlock = {},
            onNo: FunctionBlock = {},
            justReturn: Boolean = false
        ) {
            MaterialAlertDialogBuilder(context, R.style.AlertDialogTheme)
                .setTitle(title)
                .setMessage(message)
                .setNegativeButton(L.button_no) { dialog, which ->
                    onNo()
                }
                .setPositiveButton(L.button_ok) { dialog, which ->
                    onYes()
                }.apply {
                    if (!justReturn) {
                        create()
                        show()
                    }
                }
        }
    }
}