package com.sevenlearn.app.sevencaller.framework.handler

import android.content.pm.PackageManager
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import com.sevenlearn.app.sevencaller.framework.bace.BaseActivity
import com.sevenlearn.app.sevencaller.framework.bace.FunctionBlock
import com.sevenlearn.app.sevencaller.framework.bace.HelperDialog
import com.sevenlearn.app.sevencaller.framework.bace.MethodBlock
import com.sevenlearn.app.sevencaller.framework.helper.IntentHelper

class PermissionHandler constructor(
    val currentActivity: BaseActivity,
    val permissions: List<String>,
    props: MethodBlock<PermissionHandler>
) {
    companion object {
        var lastRequestCode: Int = 0
    }

    private var requestCode: Int = ++lastRequestCode
    private var activity = currentActivity
    var onGrant: FunctionBlock? = null
    var whyPermissionRequired: String = "Please grant all permissions..."
    var whyPermissionRequiredTitle: String = "Permission Required"

    init {
        this.props()
        activity.addPermissionHandler(this)
        request()
    }


    private fun canShowReason(): Boolean {
        permissions.forEach { permission ->
            if (ActivityCompat.shouldShowRequestPermissionRationale(activity, permission)) {
                return true
            }
        }
        return false
    }


    private fun request() {
        if (isGranted()) {
            return
        }
        if (canShowReason()) {
            HelperDialog.alertYesNoNew(
                currentActivity,
                title = whyPermissionRequiredTitle,
                message = whyPermissionRequired,
                onYes = {
                    requestPermission()
                },
                onNo = {
                    activity.finish()
                }
            )
        } else {
            requestPermission()
        }
    }


    private fun requestPermission() {
        ActivityCompat.requestPermissions(activity, permissions.toTypedArray(), requestCode)
    }


    private fun isGranted(): Boolean {

        permissions.forEach { permission ->
            if (PackageManager.PERMISSION_GRANTED != ContextCompat.checkSelfPermission(
                    currentActivity,
                    permission
                )
            ) {
                return false
            }
        }
        return true
    }


    fun processOnPermissionResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ): Boolean {
        if (requestCode != this.requestCode) {
            return false
        }

        // It's mine
        if (grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            if (onGrant != null) {
                onGrant!!.invoke()
            }
            return true
        }
        if (canShowReason()) {
            request()
            // denied
        } else {
            // always denied

            HelperDialog.alertYesNoNew(
                currentActivity,
                title = whyPermissionRequiredTitle,
                message = whyPermissionRequired,
                onYes = {
                    IntentHelper.openAppSettings(activity)
                    activity.finish()
                },
                onNo = {
                    activity.finish()
                }
            )
        }

        return true
    }

}